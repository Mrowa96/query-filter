<?php

namespace QueryFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class Page
 * @package QueryFilter\Filter
 */
final class Page extends AbstractFilter
{
    /**
     * @inheritdoc
     */
    public function getQueryName(): string
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function applyFilter(QueryBuilder $queryBuilder, array $filters = []): QueryBuilder
    {
        $value = abs((int)$this->getValue());

        if ($value > 0) {
            /** @var AbstractFilter $filter */
            foreach ($filters as $filter) {
                if (get_class($filter) === Limit::class) {
                    $limitValue = $filter->getValue();
                    break;
                }
            }

            if (!empty($limitValue)) {
                $queryBuilder->setFirstResult(($value - 1) * $limitValue);
            }
        }

        return $queryBuilder;
    }
}
