<?php

namespace QueryFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class AbstractFilter
 * @package QueryFilter\Filter
 */
abstract class AbstractFilter
{
    /**
     * @var string|null
     */
    protected $value;

    /**
     * @var string|null
     */
    protected $originalValue;

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * @return string
     */
    abstract public function getQueryName(): string;

    /**
     * @param QueryBuilder $queryBuilder
     * @param AbstractFilter[] $filters
     * @return QueryBuilder
     */
    abstract public function applyFilter(QueryBuilder $queryBuilder, array $filters): QueryBuilder;

    /**
     * @return null|string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->originalValue = $value;

        if (strlen(trim($value)) > 0) {
            $this->value = trim($value);
        }
    }

    /**
     * @return bool
     */
    public function hasValue(): bool
    {
        return !empty($this->getValue());
    }

    /**
     * @return string|null
     */
    public function getOriginalValue(): ?string
    {
        return $this->originalValue;
    }

    /**
     * @return void
     */
    public function enable(): void
    {
        $this->enabled = true;
    }

    /**
     * @return void
     */
    public function disable(): void
    {
        $this->enabled = false;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled === true;
    }

    /**
     * @return bool
     */
    public function canBeApplied(): bool
    {
        return $this->hasValue() && $this->isEnabled();
    }
}
