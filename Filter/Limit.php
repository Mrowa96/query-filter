<?php

namespace QueryFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class Limit
 * @package QueryFilter\Filter
 */
final class Limit extends AbstractFilter
{
    /**
     * @inheritdoc
     */
    public function getQueryName(): string
    {
        return 'limit';
    }

    /**
     * @inheritdoc
     */
    public function applyFilter(QueryBuilder $queryBuilder, array $filters = []): QueryBuilder
    {
        $value = abs((int)$this->getValue());

        if ($value > 0) {
            $queryBuilder->setMaxResults($value);
        }

        return $queryBuilder;
    }
}
