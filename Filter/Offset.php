<?php

namespace QueryFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class Offset
 * @package QueryFilter\Filter
 */
final class Offset extends AbstractFilter
{
    /**
     * @inheritdoc
     */
    public function getQueryName(): string
    {
        return 'offset';
    }

    /**
     * @inheritdoc
     */
    public function applyFilter(QueryBuilder $queryBuilder, array $filters = []): QueryBuilder
    {
        $value = abs((int)$this->getValue());

        if ($value > 0) {
            $queryBuilder->setFirstResult($value);
        }

        return $queryBuilder;
    }
}
