<?php

namespace QueryFilter\Exception;

/**
 * Class MissingFilterException
 * @package QueryFilter\Exception
 */
final class MissingFilterException extends \RuntimeException
{

}
