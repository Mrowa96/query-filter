<?php

namespace QueryFilter\Service;

use Doctrine\ORM\QueryBuilder;
use QueryFilter\Filter\AbstractFilter;

/**
 * Interface QueryFilterServiceInterface
 * @package QueryFilter\Service
 */
interface QueryFilterServiceInterface
{
    /**
     * @param AbstractFilter $filter
     */
    public function registerFilter(AbstractFilter $filter): void;

    /**
     * @param string $filterClassName
     * @return QueryFilterServiceInterface
     */
    public function allowToAutoEnableFilter(string $filterClassName): QueryFilterServiceInterface;

    /**
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    public function applyFilters(QueryBuilder $queryBuilder): QueryBuilder;

    /**
     * @param string $filterClassName
     * @return AbstractFilter|null
     */
    public function getFilter(string $filterClassName): ?AbstractFilter;

    /**
     * @return AbstractFilter[]
     */
    public function getAllFilters(): array;

    /**
     * @param string $filterClassName
     * @return QueryFilterServiceInterface
     */
    public function disableFilter(string $filterClassName): QueryFilterServiceInterface;

    /**
     * @param string $filterClassName
     * @return QueryFilterServiceInterface
     */
    public function enableFilter(string $filterClassName): QueryFilterServiceInterface;

    /**
     * @param array $filtersClassName
     * @return QueryFilterServiceInterface
     */
    public function enableFilters(array $filtersClassName): QueryFilterServiceInterface;

    /**
     * @param string $filterClassName
     * @return bool
     */
    public function hasFilter(string $filterClassName): bool;
}
