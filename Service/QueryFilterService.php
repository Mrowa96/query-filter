<?php

namespace QueryFilter\Service;

use Doctrine\ORM\QueryBuilder;
use QueryFilter\Exception\MissingFilterException;
use QueryFilter\Filter\AbstractFilter;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class QueryFilterService
 * @package QueryFilter\Service
 */
final class QueryFilterService implements QueryFilterServiceInterface
{
    /**
     * @var RequestStack|null
     */
    private $requestStack;

    /**
     * @var AbstractFilter[]
     */
    private $filters = [];

    /**
     * @var string[]
     */
    private $allowedToAutoEnableFilters = [];

    /**
     * QueryFilterService constructor.
     * @param RequestStack|null $requestStack
     */
    public function __construct(?RequestStack $requestStack = null)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function registerFilter(AbstractFilter $filter): void
    {
        $this->filters[get_class($filter)] = $filter;

        $this->populateFilterValueFromRequest($filter, $this->hasAutoEnabledFilter(get_class($filter)));
    }

    /**
     * @inheritdoc
     */
    public function allowToAutoEnableFilter(string $filterClassName): QueryFilterServiceInterface
    {
        $this->allowedToAutoEnableFilters[] = $filterClassName;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function applyFilters(QueryBuilder $queryBuilder): QueryBuilder
    {
        foreach ($this->filters as $filter) {
            if ($filter->canBeApplied()) {
                $filter->applyFilter($queryBuilder, $this->filters);
            }
        }

        return $queryBuilder;
    }

    /**
     * @inheritdoc
     */
    public function getFilter(string $filterClassName): ?AbstractFilter
    {
        return $this->hasFilter($filterClassName) ? $this->filters[$filterClassName] : null;
    }

    /**
     * @inheritdoc
     */
    public function getAllFilters(): array
    {
        return $this->filters;
    }

    /**
     * @inheritdoc
     */
    public function enableFilter(string $filterClassName): QueryFilterServiceInterface
    {
        if (!$this->hasFilter($filterClassName)) {
            throw new MissingFilterException("Filter ${filterClassName} was not registered.");
        }

        $this->getFilter($filterClassName)->enable();

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function enableFilters(array $filtersClassName): QueryFilterServiceInterface
    {
        foreach ($filtersClassName as $filterClassName) {
            $this->enableFilter($filterClassName);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function disableFilter(string $filterClassName): QueryFilterServiceInterface
    {
        if (!$this->hasFilter($filterClassName)) {
            throw new MissingFilterException("Filter ${filterClassName} was not registered.");
        }

        $this->getFilter($filterClassName)->disable();

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function hasFilter(string $filterClassName): bool
    {
        return array_key_exists($filterClassName, $this->filters);
    }

    /**
     * @param AbstractFilter $filter
     */
    private function populateFilterValueFromRequest(AbstractFilter $filter, bool $enable = false): void
    {
        if (!is_null($this->requestStack) && !is_null($this->requestStack->getCurrentRequest())) {
            $request = $this->requestStack->getCurrentRequest();
            $value = $request->query->get($filter->getQueryName());

            if (!empty($value)) {
                $filter->setValue($value);

                if ($enable) {
                    $filter->enable();
                }
            }
        }
    }

    /**
     * @param string $filterClassName
     * @return bool
     */
    private function hasAutoEnabledFilter(string $filterClassName): bool
    {
        return false !== array_search($filterClassName, $this->allowedToAutoEnableFilters);
    }
}
