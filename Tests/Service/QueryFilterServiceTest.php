<?php

namespace QueryFilter\Tests\Service;

use PHPUnit\Framework\TestCase;
use QueryFilter\Filter\AbstractFilter;
use QueryFilter\Service\QueryFilterService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class QueryFilterServiceTest
 * @package QueryFilter\Tests\Service
 */
class QueryFilterServiceTest extends TestCase
{
    /**
     * @var QueryFilterService
     */
    private $service;

    /**
     * @var AbstractFilter
     */
    private $filter;

    public function setUp()
    {
        $request = $this->createMock(Request::class);
        $request->query = $this->createMock(ParameterBag::class);
        $request->query->expects($this->any())
            ->method('get')
            ->with('test')
            ->willReturn(5);

        $requestStack = $this->createMock(RequestStack::class);
        $requestStack->expects($this->any())
            ->method('getCurrentRequest')
            ->willReturn($request);

        $this->service = new QueryFilterService($requestStack);

        $this->filter = $this->getMockForAbstractClass(AbstractFilter::class);
        $this->filter->expects($this->any())
            ->method('getQueryName')
            ->willReturn('test');

        $this->service->registerFilter($this->filter);
    }

    public function testGetFilterMethodWithCorrectValue()
    {
        $this->assertTrue($this->service->hasFilter(get_class($this->filter)));
        $this->assertEquals('test', $this->service->getFilter(get_class($this->filter))->getQueryName());
        $this->assertEquals(5, $this->service->getFilter(get_class($this->filter))->getValue());
    }

    public function testGetFilterMethodWithIncorrectValue()
    {
        $this->assertFalse($this->service->hasFilter('testInvalid'));
        $this->assertEquals(null, $this->service->getFilter('testInvalid'));
    }

    public function testEnableFilterMethod()
    {
        $this->service->enableFilter(get_class($this->filter));
        $this->assertTrue($this->service->getFilter(get_class($this->filter))->isEnabled());
    }

    public function testDisableFilterMethod()
    {
        $this->service->disableFilter(get_class($this->filter));
        $this->assertFalse($this->service->getFilter(get_class($this->filter))->isEnabled());
    }
}
