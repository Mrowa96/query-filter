<?php

namespace QueryFilter\Tests\Filter;

use PHPUnit\Framework\TestCase;
use QueryFilter\Filter\AbstractFilter;

/**
 * Class AbstractFilterTest
 * @package QueryFilter\Tests\Filter
 */
class AbstractFilterTest extends TestCase
{
    /**
     * @var AbstractFilter
     */
    private $filter;

    public function setUp()
    {
        $this->filter = $this->getMockForAbstractClass(AbstractFilter::class);
        $this->filter->expects($this->any())
            ->method('getQueryName')
            ->will($this->returnValue('test'));
    }

    public function testSetValueMethodWithCorrectValue()
    {
        $this->filter->setValue(' value ');

        $this->assertEquals('value', $this->filter->getValue());
        $this->assertEquals(' value ', $this->filter->getOriginalValue());
        $this->assertTrue($this->filter->hasValue());
    }

    public function testSetValueMethodWithIncorrectValue()
    {
        $this->filter->setValue('  ');

        $this->assertEquals(null, $this->filter->getValue());
        $this->assertEquals('  ', $this->filter->getOriginalValue());
        $this->assertFalse($this->filter->hasValue());
    }

    public function testEnableMethod()
    {
        $this->filter->enable();

        $this->assertTrue($this->filter->isEnabled());
    }

    public function testDisableMethod()
    {
        $this->filter->disable();

        $this->assertFalse($this->filter->isEnabled());
    }

    public function testCanBeAppliedMethod()
    {
        $this->filter->enable();
        $this->filter->setValue('value');

        $this->assertTrue($this->filter->canBeApplied());
    }
}
