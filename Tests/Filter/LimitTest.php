<?php

namespace QueryFilter\Tests\Filter;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use QueryFilter\Filter\Limit;

/**
 * Class LimitTest
 * @package QueryFilter\Tests\Filter
 */
class LimitTest extends TestCase
{
    /**
     * @var Limit
     */
    private $filter;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        $this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->filter = new Limit();
    }

    public function testGetName()
    {
        $this->assertEquals('limit', $this->filter->getQueryName());
    }

    public function testApplyFilterMethod()
    {
        $this->filter->setValue(5);
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->setMethods(['setMaxResults'])
            ->disableOriginalConstructor()
            ->getMock();

        $queryBuilder->expects($this->once())
            ->method('setMaxResults')
            ->with(5)
            ->will($this->returnValue($queryBuilder));

        $this->filter->applyFilter($queryBuilder);
    }
}
