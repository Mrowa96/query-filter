<?php

namespace QueryFilter\Tests\Filter;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use QueryFilter\Filter\Limit;
use QueryFilter\Filter\Page;

/**
 * Class PageTest
 * @package QueryFilter\Tests\Filter
 */
class PageTest extends TestCase
{
    /**
     * @var Page
     */
    private $filter;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        $this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->filter = new Page();
    }

    public function testGetName()
    {
        $this->assertEquals('page', $this->filter->getQueryName());
    }

    public function testApplyFilterMethodWithoutLimitFilter()
    {
        $this->filter->setValue(5);
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->setMethods(['setFirstResult'])
            ->disableOriginalConstructor()
            ->getMock();

        $queryBuilder->expects($this->never())
            ->method('setFirstResult')
            ->will($this->returnValue($queryBuilder));

        $this->filter->applyFilter($queryBuilder);
    }

    public function testApplyFilterMethodWithLimitFilter()
    {
        $limitFilter = new Limit();
        $limitFilter->setValue(10);
        $this->filter->setValue(5);

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->setMethods(['setFirstResult'])
            ->disableOriginalConstructor()
            ->getMock();

        $queryBuilder->expects($this->once())
            ->method('setFirstResult')
            ->with(40)
            ->will($this->returnValue($queryBuilder));

        $this->filter->applyFilter($queryBuilder, [$limitFilter]);
    }
}
