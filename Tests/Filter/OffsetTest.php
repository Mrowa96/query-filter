<?php

namespace QueryFilter\Tests\Filter;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use QueryFilter\Filter\Offset;

/**
 * Class OffsetTest
 * @package QueryFilter\Tests\Filter
 */
class OffsetTest extends TestCase
{
    /**
     * @var Offset
     */
    private $filter;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        $this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->filter = new Offset();
    }

    public function testGetName()
    {
        $this->assertEquals('offset', $this->filter->getQueryName());
    }

    public function testApplyFilterMethod()
    {
        $this->filter->setValue(5);
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->setMethods(['setFirstResult'])
            ->disableOriginalConstructor()
            ->getMock();

        $queryBuilder->expects($this->once())
            ->method('setFirstResult')
            ->with(5)
            ->will($this->returnValue($queryBuilder));

        $this->filter->applyFilter($queryBuilder);
    }
}
