<?php

namespace QueryFilter\DependencyInjection\Compiler;

use QueryFilter\Service\QueryFilterServiceInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class QueryFilterPass
 * @package QueryFilter\DependencyInjection\Compiler
 */
final class QueryFilterPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(QueryFilterServiceInterface::class)) {
            return;
        }

        $definition = $container->findDefinition(QueryFilterServiceInterface::class);
        $taggedServices = $container->findTaggedServiceIds('query_filter.filter');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('registerFilter', [new Reference($id)]);
        }
    }
}
