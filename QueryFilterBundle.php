<?php

namespace QueryFilter;

use QueryFilter\DependencyInjection\Compiler\QueryFilterPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class QueryFilterBundle extends Bundle
{
    /**
     * @inheritdoc
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new QueryFilterPass());
    }
}
