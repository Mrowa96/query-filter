# Query Filter for Symfony

## How to use?
Inject QueryFilterService into e.g. some repository and before executing getQuery method on your queryBuilder instance
call applyFilters method from QueryFilterService.

## Example
`$this->queryFilterService->applyFilters($queryBuilder);`

## How to extend filters?
You can extend filters by creating your own classes which will extend from AbstractFilter.
Please remember about tagging your new filter with query_filter.filter
